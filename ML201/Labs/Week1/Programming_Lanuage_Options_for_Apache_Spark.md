<img src="images/IDSNlogo.png" width="300">

Objective for Exercise:
* Video on programming language options

### Programming Lanuage Options for Apache Spark (optional)

In the last video we've told you that there exist different options for programming languages using Apache Spark. We see a clear focus of the data science community on python. So we highly recommend using python. We've therefore removed the video on programming language options. In case you want to get an overview on the different programming language options, please have a look at the following video:

https://www.coursera.org/lecture/ds/programming-language-options-on-apachespark-g34Ty


## Author(s)
[Romeo Kienzler](linkedin.com/in/romeo-kienzler-089b4557)

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2020-09-16 | 2.0 | Srishti | Migrated Lab to Markdown and added to course repo in GitLab |
|   |   |   |   |
|   |   |   |   |

## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>