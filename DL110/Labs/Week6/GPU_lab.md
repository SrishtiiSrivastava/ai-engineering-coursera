<img src="images/IDSNlogo.png" width="300">

Objective for Exercise:
* How we can use graphics processing units or GPUs in PyTorch.

If you have not created a Watson service before proceed with Step 1, otherwise go to Step 2:

#### Step 1: For New Users (with no Watson service):

For this project, you will use your IBM Watson Studio account from the previous chapter.  

Go to the IBM Cloud Watson Studio page:

<a href="https://cloud.ibm.com/catalog/services/watson-studio">Click here</a>

You will see the screen in the figure below. Click the icon in the red box:

<img src="images/6.1.1.png" width="600">

Then click **Watson**, as shown below:

<img src="images/6.1.2.png" width="600">

Then click **Browse Services.**

<img src="images/6.1.3.png" width="600">

Scroll down and select **Watson Studio - Lite.**

<img src="images/6.1.4.png" width="600">

To create a Watson service using the Lite plan, click **Create**.

<img src="images/6.1.5.png" width="600">

Now click **Get Started.**

<img src="images/6.1.6.png" width="600">

After creating the service continue with Step 2.

#### Step 2: For Existing Users (who already have Watson Service):

Go to the IBM Cloud Dashboard and click **Services.**

<img src="images/6.1.7.png" width="600">

When you click on Services, all your existing services will be shown in the list. Click the **Watson Studio** service you created:

<img src="images/6.1.8.png" width="600">

Then click **Get Started.**

<img src="images/6.1.9.png" width="600">

#### Step 3: Creating a Project
Now you have to Create a project.

Click on **Create a project**:

<img src="images/6.1.10.png" width="600">

On the Create a project page, click **Create an empty project**

<img src="images/6.1.11.png" width="600">

Provide a **Project Name** and **Description**, as shown below:

<img src="images/6.1.12.png" width="600">

You must also create storage for the project.
**Click Add**

<img src="images/6.1.13.png" width="600">

On the Cloud Object Storage page, scroll down and then click **Create.**

<img src="images/6.1.14.png" width="600">

In the Confirm Creation box, click **Confirm.**

<img src="images/6.1.15.png" width="300">

On the New project page, note that the storage has been added, and then click **Create.**

<img src="images/6.1.16.png" width="600">

After creating the project continue with Step 3.

#### Step 3: Adding a Notebook to the Project:

You need to add a Notebook to your project. Click **Add to project**.

<img src="images/6.1.17.png" width="600">

In the list of asset types, click **Notebook**:

<img src="images/6.1.18.png" width="600">

On the New Notebook page, enter a name for the notebook, and then click **From URL**.
Copy this link:

Click here

Paste it into the **Notebook URL** box, and then click **Create Notebook**.

<img src="images/6.1.19.png" width="600">

You will see this Notebook:

<img src="images/6.1.20.png" width="600">

Once you complete your notebook you will have to share it. Select the icon on the top right a marked in red in the image below, a dialogue box should open, select the option all content excluding sensitive code cells.

<img src="images/6.1.21.png" width="600">

<img src="images/6.1.22.png" width="600">

23


## Author(s)
[Joseph Santarcangelo](linkedin.com/in/joseph-s-50398b136)

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2020-09-16 | 2.0 | Srishti | Migrated Lab to Markdown and added to course repo in GitLab |
|   |   |   |   |
|   |   |   |   |

## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>












