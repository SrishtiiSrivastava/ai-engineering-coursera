<img src="images/IDSNlogo.png" width="300">

Objective for Exercise:
* How to use the GPU in Watson Studio.

#### How to use the GPU in Watson Studio

Navigate to your instance of Watson Studio using IBM Cloud

<img src="images/6.2.1.png" width="600">

To make sure that GPU is enabled on this isntance of Watson Studio press the plan button on the top left. You should see the check mark on the right associated with the Standard Plan which will allow you to use Nvidia GPU.

<img src="images/6.2.2.png" width="600">

Using the Manage button naviage back to the starting page and press the get started button. Select your project.

<img src="images/6.2.3.png" width="600">

In the assets you can create a new notebook using an environment with a GPU. Press the new notebook button.

<img src="images/6.2.4.png" width="600">

Using the runtime selection select an environment with a GPU. These can be identified by the word GPU or Nvidia.

<img src="images/6.2.5.png" width="600">

After selecting the GPU environment you can create the notebook by giving it a name and pressing create button on the bottom right:

<img src="images/6.2.6.png" width="600">

You can also change the environment of an existing notebook to use a GPU:

<img src="images/6.2.7.png" width="600">

You may need to unlock the notebook to do this.

<img src="images/6.2.8.png" width="600">

You also may need to stop the kernel.

<img src="images/6.2.9.png" width="600">

After doing this you will see the option to change the environment.

<img src="images/6.2.10.png" width="600">

Change the environment to one that uses a GPU identified by the word GPU or Nvidia. Then press the associate button.

<img src="images/6.2.11.png" width="600">

The environment change has already been made. The change will be visible in the asset page after you edit the notebook. A GPU is now enabled in your Watson Studio instance and you may use it as if it was a personal GPU. For more info about the actual usage of the GPU use the notebook provided.

<img src="images/6.2.12.png" width="600">

## Author(s)
[Joseph Santarcangelo](linkedin.com/in/joseph-s-50398b136)

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2020-09-16 | 2.0 | Srishti | Migrated Lab to Markdown and added to course repo in GitLab |
|   |   |   |   |
|   |   |   |   |

## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>













